# TestFrontAirWeb

This project has been made for the airWeb company, as part of a technical test.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.1.

## Development server

Download or fetch the project.

Run `npm i` to install the node modules.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng serve --configuration development` to be able to debug locally.

Run the backend previously given.

## Project

This project has been made in ~ 6 to 8 hours.

As part of the given tasks, every API routes are used.

A main page display cards of every products.

The tabs in the nav-bar redirects to the specified category and the products corresponding.

A little searchbar is also given in this component.

A page is provided for every products also.

Once items added to cart, the cart is available with products in it.

A falsy checkout page is provided.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


