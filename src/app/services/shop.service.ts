import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Iproduct } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  public addedToCart: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public items: Iproduct[] = [];
  public isAddedToCart: boolean = false;

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getProducts() {
    return this.http.get(this.baseUrl + "products")
  }

  public getProduct(id: number) {
    return this.http.get(this.baseUrl + 'products/' + id);
  }

  public getCategories() {
    return this.http.get(this.baseUrl + 'categories');
  }

  public getCategory(id: number) {
    return this.http.get(this.baseUrl + 'categories/' + id);
  }

  public addToCart(product: Iproduct) {
    this.isAddedToCart = true;
    this.items.push(product);
  }

  getItems() {
    return this.items;
  }

  clearCart() {
    this.items = [];
    this.isAddedToCart = false;
    return this.items;
  }
}
