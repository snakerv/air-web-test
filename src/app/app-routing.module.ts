import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasketComponent } from './component/basket/basket.component';
import { CategoryComponent } from './component/category/category.component';
import { CheckoutComponent } from './component/checkout/checkout.component';
import { LandingPageComponent } from './component/landing-page/landing-page.component';
import { ProductComponent } from './component/product/product.component';

const routes: Routes = [
  {path: '', redirectTo: 'catalog', pathMatch: 'full'},
  { path: 'catalog', component: LandingPageComponent },
  { path: 'basket', component: BasketComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'product/:id', component: ProductComponent },
  { path: 'category/:id', component: CategoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
