import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Iproduct } from '../../../interfaces/product';
import { ShopService } from '../../../services/shop.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input() product: Iproduct;
  public quantity: number = 0;

  constructor(
    private shopService: ShopService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if(this.product.thumbnail_url == null) {
      this.product.thumbnail_url = "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png"
    }
  }

  public addToCart(product: Iproduct) {
    this.shopService.addToCart(product);
    this.quantity ++;
    window.alert('Article bien ajouté au panier!');
  }

  public getProductDetail(id) {
    this.router.navigateByUrl("/product/" + id);
  }

}
