import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Iproduct } from '../../interfaces/product';
import { ShopService } from '../../services/shop.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
  public product: Iproduct;
  public productList: Iproduct[];
  public productsSelected: Iproduct[];
  public currentId: any;
  private routeSub: Subscription;

  constructor(
    public shopService: ShopService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id'];
      this.getProducts();
    });
  }

  public getProduct(id: number) {
    this.shopService.getProduct(id).subscribe((product: Iproduct) => {
      this.product = product;
    })
  }

  public getProducts() {
    this.shopService.getProducts().subscribe((prod: Iproduct[]) => {
      this.productList = prod;
      this.retrieveById();
    })
  }

  public retrieveById() {
    let newProducts = [];
    for(let i = 0; i <= this.productList.length; i++) {
      if(this.currentId == this.productList[i]?.category_id) {
        newProducts.push(this.productList[i]);
        this.productsSelected = newProducts;
      }
    }
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

}
