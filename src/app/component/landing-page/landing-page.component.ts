import { Component, OnInit } from '@angular/core';
import { Iproduct } from '../../interfaces/product';
import { ShopService } from '../../services/shop.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  public productList: Iproduct[];

  constructor(
    public shopService: ShopService,
  ) { }

  ngOnInit(): void {
    this.getProducts()
  }

  public getProducts() {
    this.shopService.getProducts().subscribe((products: Iproduct[]) => {
      this.productList = products;
    })
  }

}
