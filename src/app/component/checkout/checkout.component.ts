import { Component, OnInit, ViewChild } from '@angular/core';
import {FormGroup,FormControl,Validators,FormArray} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ShopService } from '../../services/shop.service';
import { BasketComponent } from '../basket/basket.component';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  constructor(
    public router: Router,
    public shopService: ShopService
  ) {}

  public totalPrice: string;
  genders=['male','female'];
  SignupForm: FormGroup;

  ngOnInit(){
    this.totalPrice = localStorage.getItem("totalPrice");
    this.SignupForm = new FormGroup({
      firstname: new FormControl(),
      lastname: new FormControl(),
      email: new FormControl(),
      gender: new FormControl(),
      adress: new FormControl(),
      creditCardNumber: new FormControl(),
      expireMonth: new FormControl(),
      expireYear: new FormControl(),
      cvv: new FormControl(),
    });
  }

  onSubmit(){
    localStorage.setItem("totalPrice", "0");
    window.alert('Articles achetés avec succés');
    this.shopService.clearCart();
    this.router.navigateByUrl('/catalog');
  }

}
