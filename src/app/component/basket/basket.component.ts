import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Iproduct } from 'src/app/interfaces/product';
import { ShopService } from '../../services/shop.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit, OnDestroy {
  public products = this.shopService.getItems();
  basketSubscription: any;
  totalPrice: number;

  constructor(
    private shopService: ShopService,
    private router: Router,
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.basketSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
    this.total();
  }

  ngOnDestroy() {
    if (this.basketSubscription) {
      this.basketSubscription.unsubscribe();
    }
  }

  public deleteAll() {
    this.shopService.clearCart();
    window.alert('Panier effacé!');
    this.router.navigateByUrl('/basket');
  }

  public checkout() {
    this.router.navigateByUrl('/checkout');
  }

  public total() {
    let newTotal = this.products.map(x => x.price);
    this.totalPrice = newTotal.reduce((a, b) => a + b, 0);
    localStorage.setItem("totalPrice", this.totalPrice.toString());
  }

}
