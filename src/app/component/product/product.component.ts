import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Iproduct } from '../../interfaces/product';
import { ShopService } from '../../services/shop.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  public currentId: any;
  private routeSub: Subscription;
  public product: Iproduct;
  public quantity: number = 0;

  constructor(
    private route: ActivatedRoute,
    private shopService: ShopService,
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.currentId = params['id'];
      this.getProduct(this.currentId);
    });
  }

  private getProduct(id) {
    this.shopService.getProduct(id).subscribe((product: Iproduct) => {
      this.product = product;
    })
  }

  public addToCart(product: Iproduct) {
    this.shopService.addToCart(product);
    this.quantity ++;
    window.alert('Article bien ajouté au panier!');
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

}
