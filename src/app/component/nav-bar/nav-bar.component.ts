import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Iproduct } from 'src/app/interfaces/product';
import { Icategory } from '../../interfaces/category';
import { ShopService } from '../../services/shop.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})

export class NavBarComponent implements OnInit {
  public navLinks: any[];
  public items: Iproduct[];
  public allItems: Iproduct[];
  public searchTerm: string = "";

  constructor(
    public shopService: ShopService,
    public router: Router
  ) { }

  ngOnInit() {
    this.getCategories();
    this.getItems();
  }

  private getCategories() {
    this.shopService.getCategories().subscribe((data: Icategory[]) => {
      this.navLinks = data;
    })
  }

  public search(value: string): void {
    this.items = this.allItems.filter((val) =>
      val.label.toLowerCase().includes(value)
    );
  }

  private getItems() {
    this.shopService.getProducts().subscribe((data: Iproduct[]) => {
      this.items = data;
      this.allItems = data;
    })
  }

  public goToPage(id) {
    this.router.navigateByUrl('/category/' + id);
  }

  public goHome() {
    this.router.navigateByUrl('/catalog');
  }

  public goToCart() {
    this.router.navigateByUrl('/basket');
  }

  public getProductDetail(id) {
    this.searchTerm = "";
    this.router.navigateByUrl("/product/" + id);
  }

}
