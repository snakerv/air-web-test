export interface Iproduct {
  description: string;
  category_id: string;
  id: string;
  label: string;
  price: number;
  thumbnail_url: string;
}
