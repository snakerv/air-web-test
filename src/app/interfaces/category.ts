export interface Icategory {
  description: string;
  id: string;
  index: number;
  label: string;
}
